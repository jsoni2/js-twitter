package com.parse.starter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;
import java.util.Map;

public class FeedListAdapter extends
        RecyclerView.Adapter<FeedListAdapter.FeedViewHolder> {

    private OnItemClickListener mListener;

    public interface OnItemClickListener {
        void onItemClick(int position);
        void onLikeClick(int position, TextView feedTweetView, TextView feedTweetFromView);
        void onReplyClick(int position, TextView feedTweetView, TextView feedTweetFromView);
        void onRetweetClick(int position, TextView feedTweetView, TextView feedTweetFromView);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

    private  final List<Map<String, String>> fFeedList;
    private LayoutInflater fInflater;

    public FeedListAdapter(Context context, List<Map<String, String>> fFeedList) {
        fInflater = LayoutInflater.from(context);
        this.fFeedList = fFeedList;
    }

    @Override
    public FeedListAdapter.FeedViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View fItemView = fInflater.inflate(R.layout.feedlist_item, parent, false);
        return new FeedViewHolder(fItemView, this, mListener);
    }

    @Override
    public void onBindViewHolder(FeedListAdapter.FeedViewHolder holder, int position) {
        Map<String, String> fCurrent = fFeedList.get(position);

        holder.feedTweetView.setText(fCurrent.get("content"));
        holder.feedTweetFromView.setText(fCurrent.get("username"));
    }

    @Override
    public int getItemCount() {
        return fFeedList.size();
    }

    public class FeedViewHolder extends RecyclerView.ViewHolder {
        public final TextView feedTweetView;
        public final TextView feedTweetFromView;
        public final Button likeButton, replyButton, retweetButton;
        final FeedListAdapter fAdapter;

        public FeedViewHolder(View itemView, FeedListAdapter adapter,final OnItemClickListener listener) {
            super(itemView);
            feedTweetView = (TextView) itemView.findViewById(android.R.id.text1);
            feedTweetFromView = (TextView) itemView.findViewById(android.R.id.text2);
            likeButton = (Button) itemView.findViewById(R.id.like_button);
            replyButton = (Button) itemView.findViewById(R.id.reply_button);
            retweetButton = (Button) itemView.findViewById(R.id.retweet_button);

            this.fAdapter = adapter;

            itemView.setOnClickListener(new View.OnClickListener( ) {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position!= RecyclerView.NO_POSITION) {
                            listener.onItemClick(position);
                        }
                    }
                }
            });

            likeButton.setOnClickListener(new View.OnClickListener( ) {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position!= RecyclerView.NO_POSITION) {
                            listener.onLikeClick(position, feedTweetView, feedTweetFromView);
                        }
                    }
                }
            });

            replyButton.setOnClickListener(new View.OnClickListener( ) {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position!= RecyclerView.NO_POSITION) {
                            listener.onReplyClick(position, feedTweetView, feedTweetFromView);
                        }
                    }
                }
            });

            retweetButton.setOnClickListener(new View.OnClickListener( ) {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position!= RecyclerView.NO_POSITION) {
                            listener.onRetweetClick(position, feedTweetView, feedTweetFromView);

                        }
                    }
                }
            });


        }
    }
}
