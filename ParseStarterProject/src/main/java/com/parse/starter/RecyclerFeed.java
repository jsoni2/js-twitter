package com.parse.starter;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RecyclerFeed extends AppCompatActivity {

    private RecyclerView fRecyclerView;
    private FeedListAdapter fAdapter;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = new MenuInflater(this);
        menuInflater.inflate(R.menu.tweet_menu, menu);

        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.tweet) {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Send a Tweet");

            final EditText tweetContentEditText = new EditText(this);
            builder.setView(tweetContentEditText);

            builder.setPositiveButton("Send", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                    Toast.makeText(RecyclerFeed.this, tweetContentEditText.getText().toString(), Toast.LENGTH_SHORT).show();

                    ParseObject tweet = new ParseObject("tweet");
                    tweet.put("username", ParseUser.getCurrentUser().getUsername());
                    tweet.put("tweet", tweetContentEditText.getText().toString());

                    tweet.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {

                            if (e == null) {
                                Toast.makeText(RecyclerFeed.this, "Tweeted Successfully", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(RecyclerFeed.this, "Tweet failed - please try again later", Toast.LENGTH_SHORT).show();
                            }

                        }
                    });
                }
            });

            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                    dialogInterface.cancel();
                    Toast.makeText(RecyclerFeed.this, "Let's not Tweet!", Toast.LENGTH_SHORT).show();

                }
            });

            builder.show();

        } else if (item.getItemId() == R.id.logout){

            ParseUser.logOut();

            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);

        } else if (item.getItemId() == R.id.follow_users) {

            Intent intent = new Intent(getApplicationContext(), FollowActivity.class);
            startActivity(intent);

        }

        return super.onOptionsItemSelected(item);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_feed);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Your Feed");

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener( ) {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(RecyclerFeed.this);
                builder.setTitle("Send a Tweet");

                final EditText tweetContentEditText = new EditText(RecyclerFeed.this);
                builder.setView(tweetContentEditText);

                builder.setPositiveButton("Send", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        Toast.makeText(RecyclerFeed.this, tweetContentEditText.getText().toString(), Toast.LENGTH_SHORT).show();

                        ParseObject tweet = new ParseObject("tweet");
                        tweet.put("username", ParseUser.getCurrentUser().getUsername());
                        tweet.put("tweet", tweetContentEditText.getText().toString());

                        tweet.saveInBackground(new SaveCallback() {
                            @Override
                            public void done(ParseException e) {

                                if (e == null) {
                                    Toast.makeText(RecyclerFeed.this, "Tweeted Successfully", Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(RecyclerFeed.this, "Tweet failed - please try again later", Toast.LENGTH_SHORT).show();
                                }

                            }
                        });
                    }
                });

                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        dialogInterface.cancel();
                        Toast.makeText(RecyclerFeed.this, "Let's not Tweet!", Toast.LENGTH_SHORT).show();

                    }
                });

                builder.show();
            }
        });

        final List<Map<String, String>> tweetData = new ArrayList<Map<String, String>>();

        if (ParseUser.getCurrentUser().get("isFollowing") == null) {

            List<String> emptyList = new ArrayList<>();
            ParseUser.getCurrentUser().put("isFollowing", emptyList);

        }

        fRecyclerView = (RecyclerView) findViewById(R.id.recyclerview);

        ParseQuery<ParseObject> query = ParseQuery.getQuery("tweet");

        query.whereContainedIn("username", ParseUser.getCurrentUser().getList("isFollowing"));
        query.orderByDescending("createdAt");
        query.setLimit(20);

        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                if (e == null) {

                    if (list.size() > 0) {

                        for (ParseObject tweet: list) {
                            if (tweet.get("likes") == null) {

                                List<String> emptyList = new ArrayList<>();
                                tweet.put("likes", emptyList);
                                tweet.saveInBackground();

                            }

                            if (tweet.get("replies") == null) {

                                List<String> emptyList = new ArrayList<>();
                                tweet.put("replies", emptyList);
                                tweet.saveInBackground();

                            }

                            System.out.println(tweet.getObjectId());

                            Map<String, String> tweetInfo = new HashMap<String, String>();

                            tweetInfo.put("content", tweet.getString("tweet"));
                            tweetInfo.put("username", tweet.getString("username"));

                            tweetData.add(tweetInfo);
                        }

                        fAdapter = new FeedListAdapter(RecyclerFeed.this, tweetData);
                        fRecyclerView.setAdapter(fAdapter);
                        fRecyclerView.setLayoutManager(new LinearLayoutManager(RecyclerFeed.this));
                        fAdapter.setOnItemClickListener(new FeedListAdapter.OnItemClickListener( ) {
                            @Override
                            public void onItemClick(int position) {
                                Toast.makeText(RecyclerFeed.this, "Item "+position+ " clicked", Toast.LENGTH_SHORT).show( );
                            }

                            @Override
                            public void onLikeClick(int position, TextView feedTweetView, TextView feedTweetFromView) {
                                Toast.makeText(RecyclerFeed.this, "Like Clicked at" +position+ "!", Toast.LENGTH_SHORT).show( );
                                final ParseQuery<ParseObject> likequery = ParseQuery.getQuery("tweet");
                                likequery.whereEqualTo("username", feedTweetFromView.getText().toString());
                                likequery.whereEqualTo("tweet", feedTweetView.getText().toString());

                                likequery.findInBackground(new FindCallback<ParseObject>( ) {
                                    @Override
                                    public void done(List<ParseObject> list, ParseException e) {
                                        if (e == null) {
                                            for (ParseObject tweet: list){
                                                Toast.makeText(RecyclerFeed.this, tweet.getObjectId(), Toast.LENGTH_SHORT).show( );
                                                likequery.getInBackground(tweet.getObjectId( ), new GetCallback<ParseObject>( ) {
                                                    @Override
                                                    public void done(ParseObject parseObject, ParseException e) {
                                                        parseObject.getList("likes").add(ParseUser.getCurrentUser().getUsername());
                                                        List newLikeList = parseObject.getList("likes");
                                                        parseObject.remove("likes");
                                                        parseObject.put("likes", newLikeList);
                                                        parseObject.saveInBackground();
                                                    }
                                                });
                                            }
                                        } else {
                                            Toast.makeText(RecyclerFeed.this, "something went wrong in like tweet", Toast.LENGTH_SHORT).show( );
                                        }
                                    }
                                });
                            }

                            @Override
                            public void onReplyClick(int position,final TextView feedTweetView,final TextView feedTweetFromView) {
                                Toast.makeText(RecyclerFeed.this, "Reply Clicked at" +position+ "!", Toast.LENGTH_SHORT).show( );

                                AlertDialog.Builder builder = new AlertDialog.Builder(RecyclerFeed.this);
                                builder.setTitle("Reply on Tweet");

                                final EditText tweetContentEditText = new EditText(RecyclerFeed.this);
                                builder.setView(tweetContentEditText);

                                builder.setPositiveButton("Reply", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {

                                        Toast.makeText(RecyclerFeed.this, tweetContentEditText.getText().toString(), Toast.LENGTH_SHORT).show();

                                        final ParseQuery<ParseObject> replyquery = ParseQuery.getQuery("tweet");
                                        replyquery.whereEqualTo("username", feedTweetFromView.getText().toString());
                                        replyquery.whereEqualTo("tweet", feedTweetView.getText().toString());

                                        replyquery.findInBackground(new FindCallback<ParseObject>( ) {
                                            @Override
                                            public void done(List<ParseObject> list, ParseException e) {
                                                if (e == null) {
                                                    for (ParseObject tweet: list){
                                                        Toast.makeText(RecyclerFeed.this, tweet.getObjectId(), Toast.LENGTH_SHORT).show( );
                                                        replyquery.getInBackground(tweet.getObjectId( ), new GetCallback<ParseObject>( ) {
                                                            @Override
                                                            public void done(ParseObject parseObject, ParseException e) {
                                                                parseObject.getList("replies").add(ParseUser.getCurrentUser().getUsername() +":"+tweetContentEditText.getText().toString());
                                                                List newLikeList = parseObject.getList("replies");
                                                                parseObject.remove("replies");
                                                                parseObject.put("replies", newLikeList);
                                                                parseObject.saveInBackground();
                                                            }
                                                        });
                                                    }
                                                } else {
                                                    Toast.makeText(RecyclerFeed.this, "something went wrong in like tweet", Toast.LENGTH_SHORT).show( );
                                                }
                                            }
                                        });

                                    }
                                });

                                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {

                                        dialogInterface.cancel();
                                        Toast.makeText(RecyclerFeed.this, "Let's not Reply!", Toast.LENGTH_SHORT).show();

                                    }
                                });

                                builder.show();

                            }

                            @Override
                            public void onRetweetClick(int position, TextView feedTweetView, TextView feedTweetFromView) {
                                Toast.makeText(RecyclerFeed.this, "Retweet Clicked at" +position+ "!", Toast.LENGTH_SHORT).show( );
                                ParseObject retweet = new ParseObject("tweet");
                                retweet.put("username", ParseUser.getCurrentUser().getUsername());
                                retweet.put("tweet", feedTweetView.getText().toString());
                                retweet.put("retweetFrom", feedTweetFromView.getText().toString());
                                retweet.saveInBackground(new SaveCallback( ) {
                                    @Override
                                    public void done(ParseException e) {
                                        if (e == null) {
                                            Toast.makeText(RecyclerFeed.this, "Retweeted Successfully", Toast.LENGTH_SHORT).show();
                                        } else {
                                            Toast.makeText(RecyclerFeed.this, "Retweet failed - please try again later", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });

                            }
                        });

                    }
                    else {
                        Toast.makeText(RecyclerFeed.this, "Please, First follow people to watch feed!", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getApplicationContext(), FollowActivity.class);
                        startActivity(intent);
                    }

                }
            }
        });

    }

}
